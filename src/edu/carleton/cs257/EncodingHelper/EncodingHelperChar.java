package edu.carleton.cs257.EncodingHelper;

import java.nio.ByteBuffer;
import java.nio.charset.*;


/**
 * EncodingHelperChar
 *
 * The main support class for the EncodingHelper project in
 * CS 257, Fall 2014, Carleton College. Each object of this class
 * represents a single Unicode character. The class's methods
 * generate various representations of the character. 
 */

public class EncodingHelperChar {
    private int codePoint;
    //This loads a UTF-8 decoder that throws exceptions if the character is invalid.
    private static final CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder().
            onMalformedInput(CodingErrorAction.REPORT).onUnmappableCharacter(CodingErrorAction.REPORT);
    /*
     * The following two lines are used in our implementation of the character name lookup system.  However, Java has
     * that built in, so this is only here because the trick with the static block is kinda cool.
     */
    private static final String[] names = new String[1114110];
    private static final String UNICODE_FILE_PATH = "UnicodeData.txt";

    /*
     * The static block is run immediately prior to the first time the class is referenced.  This way we can load the
     * unicode data without needing to check if it is loaded
     * later on.
     */
    /*
     * The static block is commented out because it uses code that will only compile with Java 8, which isn't
     * necessarily installed on the lab computers yet.
     */
    /*static {
        Path path = Paths.get(UNICODE_FILE_PATH);
        try {
            Files.lines(path).forEach((a) -> names[Integer.valueOf(a.split(";")[0], 16)] = a.split(";")[1]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/


    public EncodingHelperChar(int codePoint) {
        this.codePoint = codePoint;
    }

    public EncodingHelperChar(byte[] utf8Bytes) {
        /*
         * This attempts to convert the utf8Bytes into a codePoint.  If the bytes to not represent a valid codePoint,
         * it throws a RuntimeException, which, because it cannot be caught, ends the program without necessitating
         * a change in the method signature.
         */
        try {
            codePoint = decoder.decode(ByteBuffer.wrap(utf8Bytes)).charAt(0);
        }
        catch (CharacterCodingException e) {
            System.out.println("Invalid character detected.  Please enter only codepoints that satisfy the UTF-8 encoding specification.");
            throw new RuntimeException();
        }
        /*
        int j = 7;
        for (; j >= 0; j--)
            if (((utf8Bytes[0] >> j) & 1) == 0) //If this is true, we've found the highest-order zero
                break;
        j = 8 - j;
        if (utf8Bytes.length != j - 1) //This sequence is invalid
            throw new RuntimeException();
        codePoint = (byte) (utf8Bytes[0] << j) >> j;
        for (int i = 1; i < utf8Bytes.length; i++) {
            codePoint <<= 6;
            codePoint |= (byte) (utf8Bytes[i] & 63); //This wipes the 10 prefix from the byte and adds the rest to the code point.
        }
        */
    }

    public EncodingHelperChar(char ch) {
        /*
         * Java automatically casts between characters and integers.  While there are methods that provide additional
         * security, they return the result of the casting operation that the following line performs in the case of
         * single characters, so there isn't any point in using them for this case.
         */
        codePoint = ch;
    }

    public int getCodePoint() {
        return this.codePoint;
    }

    public void setCodePoint(int codePoint) {
        if (!Character.isValidCodePoint(codePoint))
            throw new RuntimeException();
        this.codePoint = codePoint;
    }

    /**
     * Converts this character into an array of the bytes in its UTF-8 representation.
     * For example, if this character is a lower-case letter e with an acute accent, 
     * whose UTF-8 form consists of the two-byte sequence C3 A9, then this method returns
     * a byte[] of length 2 containing C3 and A9.
     *
     * @return the UTF-8 byte array for this character
     */
    public byte[] toUTF8Bytes() {
        /*
        byte[] bytes;
        //First we determine how many bytes we'll need
        if (codePoint < 128)
            bytes = new byte[1];
        else if (codePoint < 2048)
            bytes = new byte[2];
        else if (codePoint < 65536)
            bytes = new byte[3];
        else if (codePoint < 2097152)
            bytes = new byte[4];
        else if (codePoint < 67108864)
            bytes = new byte[5];
        else
            bytes = new byte[6];

        int cp = codePoint; //Store the code point in a temporary variable so we can perform some bit-shift operations
        for (int i = bytes.length - 1; i > 0; i--) {
            //128 is equal to 10000000 in binary, and 63 = 111111
            //Performing a bitwise and operation on cp with 63, we get just the 6 lowest-order bits of cp
            //By performing a bitwise or operation on 128 and and the 6 lowest-order bits of cp, we get the continuation byte.
            bytes[i] = (byte) (128 | ((byte) cp & 63));
            cp >>= 6; //Last, we shift cp down 6 bits to put it into position to form the next code-point.
        }

        if (bytes.length > 1)
            //If this is a multi-byte code, this pads the first byte with the appropriate number of 1's.
            for (int i = 0; i < bytes.length; i++)
                bytes[0] = (byte) ((bytes[0] << 1) | 1);
        bytes[0] <<= 8 - bytes.length; //This shifts the leading 1's so that the first bit in this code is the first leading 1.
        bytes[0] = (byte) (bytes[0] | (byte) cp); //We don't need to mask any of the remaining bits in cp because we know that they are all 0's.
        */
        /*
         * In Java, char cannot hold all unicode characters.  Therefore, in order to avoid an overflow error when
         * converting the codePoint to a String, we are instead using the String constructor that converts the codePoint
         * directly to a String, thereby bypassing the unsafe method.
         */
        return new String(new int[]{codePoint}, 0, 1).getBytes();
    }

    /**
     * Generates the conventional 4-digit hexadecimal code point notation for this character.
     * For example, if this character is a lower-case letter e with an acute accent,
     * then this method returns the string U+00E9 (no quotation marks in the returned String).
     *
     * @return the U+ string for this character
     */
    public String toCodePointString() {
        String code = Integer.toHexString(codePoint).toUpperCase();
        while (code.length() < 4)
            code = "0" + code;
        return "U+" + code;
    }

    /**
     * Generates a hexadecimal representation of this character suitable for pasting into a
     * string literal in languages that support hexadecimal byte escape sequences in string
     * literals (e.g. C, C++, and Python). For example, if this character is a lower-case
     * letter e with an acute accent (U+00E9), then this method returns the
     * string \xC3\xA9. Note that quotation marks should not be included in the returned String.
     *
     * @return the escaped hexadecimal byte string
     */
    public String toUTF8StringWithoutQuotes() {
        byte[] utf8Bytes = toUTF8Bytes();
        String output = "";
        for (int i = 0; i < utf8Bytes.length; i++)
            output = output + Integer.toHexString(utf8Bytes[i] & 255);
        return output.toUpperCase().replaceAll("(..)", "\\\\x$1"); //The regex expression replaces every two characters with \x<characters>
    }

    /**
     * Generates the official Unicode name for this character.
     * For example, if this character is a lower-case letter e with an acute accent,
     * then this method returns the string "LATIN SMALL LETTER E WITH ACUTE" (without quotation marks).
     *
     * @return this character's Unicode name
     */
    public String getCharacterName() {
        /*
         * Our code for this method used the static block at the top of this file to load the unicode file.
         * If you want to test it, the code was:
         *
         * return names[codePoint];
         *
         * For it to work, you'll also need to uncomment the static block at the top of this file and compile the
         * program with Java 8.
         */
        return Character.getName(codePoint);
    }

    public String toString() {
        return new String(new int[]{codePoint}, 0, 1);
    }
}
