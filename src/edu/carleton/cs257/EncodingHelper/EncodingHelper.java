package edu.carleton.cs257.EncodingHelper;

import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Joshua Lipstone and Will Schifeling on 9/15/14.
 */

public class EncodingHelper {
    private static final CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder().
            onMalformedInput(CodingErrorAction.REPORT).onUnmappableCharacter(CodingErrorAction.REPORT);
    private static final String syntaxHelp =
            "java EncodingHelper [--inputType=type] [--outputType=type] [data]\n" +
            "\tinputtype can be any of: string [default], utf8, codepoint\n" +
            "\toutputtype can be any of: summary [default], string, utf8, codepoint\n" +
            "If the data argument is present, it is used as the input. Otherwise, input is\n" +
            "taken from standard input.";

    public static void main(String[] args) {
        String inputType = "string"; //Can be string, utf8, codepoint
        String outputType = "summary"; //Can be summary, string, utf8, codepoint
        String inp = "";
        for (int i = 0; i < args.length; i++) {
            if (args[i].toLowerCase().startsWith("--inputtype"))
                inputType = args[i].substring(args[i].toLowerCase().indexOf('=') + 1).toLowerCase();
            else if (args[i].toLowerCase().startsWith("--outputtype"))
                outputType = args[i].substring(args[i].toLowerCase().indexOf('=') + 1).toLowerCase();
            else
                inp = args[i];
        }

        if (inp == "") {
            Scanner in = new Scanner(System.in);
            inp = in.nextLine();
            in.close();
        }

        if (inp == "") { //If input is still empty
            System.out.println(syntaxHelp);
            return;
        }

        EncodingHelperChar[] input;
        switch(inputType) {
            case "string":
                input = readDecodedString(inp);
                break;
            case "utf8":
                try {
                    input = readUTF8String(inp.replaceFirst("\\A['\"](.*)['\"]\\z", "$1")); //Wipes extraneous '' and "
                } catch (CharacterCodingException e) {
                    System.out.println("Invalid character detected.  Please enter only codepoints that satisfy the UTF-8 encoding specification.");
                    return;
                }
                break;
            case "codepoint":
                input = readCodePointString(inp.replaceFirst("\\A['\"](.*)['\"]\\z", "$1")); //Wipes extraneous '' and "
                break;
            default:
                System.out.println(syntaxHelp);
                return;
        }

        switch(outputType) {
            case "summary":
                System.out.println(generateSummaryOutput(input));
                break;
            case "string":
                System.out.println(generateStringOutput(input));
                break;
            case "utf8":
                System.out.println(generateUTF8Output(input));
                break;
            case "codepoint":
                System.out.println(generateCodePointsOutput(input));
                break;
            default:
                System.out.println(syntaxHelp);
                return;
        }
    }

    /**
     * Reads a typical user input String e.g. "etre"
     * @param string the input string
     * @return an EncodingHelperChar array containing the wrapped characters from the String
     */
    public static EncodingHelperChar[] readDecodedString(String string) {
        ArrayList<EncodingHelperChar> output = new ArrayList<>();
        for (char c : string.toCharArray())
            output.add(new EncodingHelperChar(c));
        EncodingHelperChar[] out = new EncodingHelperChar[output.size()];
        for (int i = 0; i < output.size(); i++)
            out[i] = output.get(i);
        return out;
    }

    /**
     * Reads a U+xxxx-formatted String e.g. "U+00EA U+0074 U+0072 U+0065"
     * @param string the input string
     * @return an EncodingHelperChar array containing the wrapped characters from the String
     */
    public static EncodingHelperChar[] readCodePointString(String string) {
        string = string.replaceAll(" +", "");
        ArrayList<EncodingHelperChar> output = new ArrayList<>();
        for (int i = 0; i < string.length(); i+=6)
            output.add(new EncodingHelperChar(Integer.parseInt(string.substring(i + 2, i + 6), 16)));
        EncodingHelperChar[] out = new EncodingHelperChar[output.size()];
        for (int i = 0; i < output.size(); i++)
            out[i] = output.get(i);
        return out;
    }

    /**
     * Reads a \xXX-formatted String e.g. "\xC3\xAA"
     * @param string the input string
     * @return an EncodingHelperChar array containing the wrapped characters from the String
     */
    public static EncodingHelperChar[] readUTF8String(String string) throws CharacterCodingException {
        string = string.replaceAll(" +", "").replaceAll("\\\\", "0").toLowerCase();
        byte[] bytes = new byte[string.length() / 4];
        for (int i = 0; i < string.length(); i+=4)
            bytes[i / 4] = (byte) (Integer.parseInt(string.substring(i + 2, i + 4), 16) & 255);
        string = decoder.decode(ByteBuffer.wrap(bytes)).toString();
        ArrayList<EncodingHelperChar> output = new ArrayList<>();
        for (char c : string.toCharArray())
            output.add(new EncodingHelperChar(c));
        EncodingHelperChar[] out = new EncodingHelperChar[output.size()];
        for (int i = 0; i < output.size(); i++)
            out[i] = output.get(i);
        return out;
    }

    /**
     * Generates a user-readable String from an array of EncodingHelperChars
     * @param chars this uses varargs to implicitly declare the chars array, which holds the EncodingHelperChars used in this conversion
     * @return The String equivalent of the passed chars
     */
    public static String generateStringOutput(EncodingHelperChar...chars) {
        String output = "";
        for (EncodingHelperChar c : chars)
            output = output + c.toString();
        return output;
    }

    /**
     * Generates a String containing all of the codepoints in the chars array
     * @param chars this uses varargs to implicitly declare the chars array, which holds the EncodingHelperChars used in this conversion
     * @return A String containing the codepoint equivalents of the passed chars.
     */
    public static String generateCodePointsOutput(EncodingHelperChar...chars) {
        String output = "";
        for (EncodingHelperChar c : chars)
            output = output + c.toCodePointString() + " ";
        return output;
    }

    /**
     * Generates a String containing all of the codepoints in the chars array
     * @param chars this uses varargs to implicitly declare the chars array, which holds the EncodingHelperChars used in this conversion
     * @return A String containing the utf8 hex equivalents
     */
    public static String generateUTF8Output(EncodingHelperChar...chars) {
        String output = "";
        for (EncodingHelperChar c : chars)
            output = output + c.toUTF8StringWithoutQuotes();
        return output;
    }

    /**
     * Combines the three other output methods into a single output.  This also includes the Character name if the input
     * only contains one character.
     * @param chars this uses varargs to implicitly declare the chars array, which holds the EncodingHelperChars used in this conversion
     * @return A combination of the other three outputs.
     */
    public static String generateSummaryOutput(EncodingHelperChar...chars) {
        String output = (chars.length == 1 ? "Character: " : "String: ") + generateStringOutput(chars);
        output = output + "\nCode Point: " + generateCodePointsOutput(chars);
        if (chars.length == 1)
            output = output + "\nName: " + chars[0].getCharacterName();
        output = output + "\nUTF-8: " + generateUTF8Output(chars);
        return output;
    }
}
