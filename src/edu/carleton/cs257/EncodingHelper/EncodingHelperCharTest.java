package edu.carleton.cs257.EncodingHelper;

import org.junit.Before;
import org.junit.Test;

import java.nio.charset.CharacterCodingException;

import static edu.carleton.cs257.EncodingHelper.EncodingHelper.*;
import static org.junit.Assert.*;

public class EncodingHelperCharTest {
    EncodingHelperChar test;
    
    @Before
    public void setup() {
        test = new EncodingHelperChar(new byte[]{(byte) 226, (byte) 130, (byte) 172});
    }

    @Test
    public void testReadingDecodedString() {
        EncodingHelperChar[] input = readDecodedString("abc");
        assertEquals("abc", generateStringOutput(input));
    }

    @Test
    public void testReadingCodePointString() {
        EncodingHelperChar[] input = readCodePointString("U+00EA");
        assertEquals("U+00EA ", generateCodePointsOutput(input));
    }

    @Test
    public void testReadingUTF8String() {
        EncodingHelperChar[] input;
        String inp = "\\" + "xC3" + "\\xA9";
        try {
            input = readUTF8String(inp);
            assertEquals(inp, generateUTF8Output(input));
        } catch (CharacterCodingException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCharConstructor() {
        test = new EncodingHelperChar('a');
        assertEquals(97, test.getCodePoint());
    }

    @Test
    public void testBytesConstructor() {
        test = new EncodingHelperChar(new byte[] {(byte) 300, (byte)123});
        byte[] expectedBytes = new byte[] {(byte) 300, (byte) 123}, actualBytes = test.toUTF8Bytes();
        for (int i = 0; i < actualBytes.length; i++)
            assertEquals(expectedBytes[i], actualBytes[i]);
    }

    @Test
    public void testCodePointConstructor() {
        assertEquals(2, new EncodingHelperChar(2).getCodePoint());
    }

    @Test
    public void testGetCodePoint() throws Exception {
        assertEquals(8364, test.getCodePoint());
    }

    @Test
    public void testSetCodePoint() throws Exception {
        test.setCodePoint(8363);
        assertEquals(8363, test.getCodePoint());
    }

    @Test
    public void testToUTF8Bytes() throws Exception {
        byte[] expectedBytes = new byte[]{(byte) 226, (byte) 130, (byte) 172}, actualBytes = test.toUTF8Bytes();
        for (int i = 0; i < actualBytes.length; i++)
            assertEquals(expectedBytes[i], actualBytes[i]);
    }

    @Test
    public void testToCodePointString() throws Exception {
        assertEquals("U+20AC", test.toCodePointString());
    }

    @Test
    public void testToUTF8StringWithoutQuotes() throws Exception {
        assertEquals("\\xE2\\x82\\xAC", test.toUTF8StringWithoutQuotes());
    }

    @Test
    public void testGetCharacterName() throws Exception {
        assertEquals("EURO SIGN", test.getCharacterName());
    }
}